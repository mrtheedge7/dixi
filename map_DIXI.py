# -*- coding: utf-8 -*-
"""
Created on Sun May  3 19:31:24 2020

@author: chris
"""

import pandas as pd
import folium
import os
import json

url = 'https://raw.githubusercontent.com/python-visualization/folium/master/examples/data'
state_geo = './us-states.json'
state_unemployment = './US_Unemployment_Oct2012.csv'
state_data = pd.read_csv(state_unemployment)

#m = folium.Map(location=[48, -102], zoom_start=3)
#
#folium.Choropleth(
#    geo_data=state_geo,
#    name='choropleth',
#    data=state_data,
#    columns=['State', 'Unemployment'],
#    key_on='feature.id',
#    fill_color='YlGn',
#    fill_opacity=0.7,
#    line_opacity=0.2,
#    legend_name='Unemployment Rate (%)'
#).add_to(m)
#
#folium.LayerControl().add_to(m)
#
#m.save('#292_folium_chloropleth_state.html')
       

with open('./world-countries.json', 'r') as f:
    wc_data = json.load(f)
country_list = [x['properties']['name'] for x in wc_data['features']]

users = ['axver', 'bono_212', 'cobl04', 'DaveC', 'GirlsAloudFan', 'gump', 'iron yuppie', 'Jerry Dunk', 'Joey788','Lazarus', 'LemonMelon', 'LJT', 'LuckyNumber7', 'namkcuR', 'the tourist']

country_files = [x for x in os.listdir('./') if 'countries.csv' in x]


m = folium.Map(location=[0, 0], zoom_start=2,max_bounds=True, min_zoom=2, max_zoom=6)

for cfile in country_files:

    for user in users:
        if user in cfile:
            usr = user
            break
    
    if cfile == 'DIXIdata_countries.csv':
        usr = 'All'
    
    
    x1 = folium.Choropleth(
        geo_data='world-countries-adjusted.json',
        name=usr,
        data=pd.read_csv(cfile),
        columns=['Country', 'Count'],
        key_on='feature.properties.name',
        fill_color='YlOrRd',
        fill_opacity=0.7,
        line_opacity=0.2,
        legend_name='DIXIiii',
        nan_fill_opacity=0.1,
        nan_fill_color='cyan',
        highlight=True,
        overlay=False,
    )
    x1.add_to(m)
    
    x2 = folium.Choropleth(
        geo_data='world-countries-adjusted.json',
        name=usr + ' (Pop. Adjusted)',
        data=pd.read_csv(cfile),
        columns=['Country', 'PopAdjusted'],
        key_on='feature.properties.name',
        fill_color='YlOrRd',
        fill_opacity=0.7,
        line_opacity=0.2,
        legend_name='DIXIiii',
        nan_fill_opacity=0.1,
        nan_fill_color='cyan',
        highlight=True,
        overlay=False
    )
    x2.add_to(m)
    

m._children.pop('openstreetmap')
for key in m._children:
    for key2 in m._children[key]._children:
        
        if key2.startswith('color_map'):
            del(m._children[key]._children[key2])
    
        if key.startswith('choropleth'):

            if key2.startswith('geo_json'):
                k1, k2 = key, key2
                
                for user in users:
                    if user in m._children[k1].__dict__['layer_name']:
                        usr = user
                        fields=['name', usr +' artists', ]
                        alias=['Country:', usr + ' Artists:']
                if 'All' in m._children[k1].__dict__['layer_name']:
                    fields=['name', 'users-count', 'artists-count', 'total-hits', 'users']
                    alias = ['Country:','User Count:', 'Unique Artists:', 'Total Selections:', 'Users:']
                folium.GeoJsonTooltip(fields=fields, aliases=alias, localize=True).add_to(m._children[k1]._children[k2])

folium.LayerControl().add_to(m)



m.save('#DIXI_map_countries.html')