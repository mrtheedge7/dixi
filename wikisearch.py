# -*- coding: utf-8 -*-
"""
Created on Sun May  3 17:21:15 2020

@author: chris
"""

import wikipedia
import pycountry

#a_pages = wikipedia.search("Dave Marsh", results)
#a_name = a_pages[0]
#artist = wikipedia.page("Dave Marsh (musician)")
#s_html = artist.html()

def try_wiki(artist_name):

    print("\t","Scraping for...", artist_name)
    
    a_pages = wikipedia.search(artist_name)
    a_name = None
    for page in a_pages:
        if "band" in page.lower():
            a_name = page
            break
        elif "musician" in page.lower():
            a_name = page
            break
        elif "singer" in page.lower():
            a_name = page
            break
        elif "artist" in page.lower():
            a_name = page
            break
    
    if not a_name:
        a_name = a_pages[0]        
    
    artist = wikipedia.page(a_name, auto_suggest=False)
    s_html = artist.html()
    try:
        i1 = s_html.index('<table class="infobox vcard')
        s_html = s_html[i1:]
        i2 = s_html.index("</table>")
        s_html = s_html[0:i2]
    except ValueError:
        try:
            i1 = s_html.index('<table class="infobox biography vcard')
            s_html = s_html[i1:]
            i2 = s_html.index("</table>")
            s_html = s_html[0:i2]
        except ValueError:
            i1 = s_html.index('infobox vcard plainlist')
            s_html = s_html[i1:]
            i2 = s_html.index("</table>")
            s_html = s_html[0:i2]


    try:
        i3 = s_html.index("Origin")
        s_html = s_html[i3:]
        i4 = s_html.index("<td>")
        s_html = s_html[i4:]
        i5 = s_html.index("</td>")
        s_html = s_html[0:i5].lower()
    except ValueError:
        i3 = s_html.index("Born")
        s_html = s_html[i3:]
        i4 = s_html.index("<td>")
        s_html = s_html[i4:]
        i5 = s_html.index("</td>")
        s_html = s_html[0:i5].lower()
    
    countries = [x.name.lower() for x in pycountry.countries] + ['northern ireland','scotland', 'england', 'wales', 'u.s.', 'us'] + [x.alpha_3.lower() for x in pycountry.countries]
    
    match = None
    for country in countries:
        
        if country in s_html:
            match = country
            break
    
    return match