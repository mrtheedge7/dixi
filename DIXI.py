# -*- coding: utf-8 -*-
"""
Created on Sun May  3 13:10:35 2020

@author: chris
"""

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import json
import wikisearch

users = {"DaveC":{"playlist-uri":"spotify:playlist:2zHfz5RjGMDuLWU67Hjnqu"},
         "Lazarus":{"playlist-uri": "spotify:playlist:6ikutmY1rNFgTsVcNPqz8Q"},
         "LemonMelon":{"playlist-uri":"spotify:playlist:7hWYUr9bjY9bLuNunvS3mk"},
         "Jerry Dunk":{"playlist-uri":"spotify:playlist:4G4bYd1aW7AbyF6vHV5ZiJ"},
         "axver":{"playlist-uri":"spotify:playlist:1nzkGj7OEINcioqknbUJWG"},
         "gump":{"playlist-uri":"spotify:playlist:2iTDdGdpFk2fChJ8YsJVna"},
         "LuckyNumber7":{"playlist-uri":"spotify:playlist:5AHfHIUB9TZQw6oUaN4rmF"},
         "the tourist":{"playlist-uri":"spotify:playlist:4c9TwgH7IY2CiSi1GDmcyC"},
         "namkcuR":{"playlist-uri":"spotify:playlist:15bGW1KiB483OpGtttDuSI"},
         "GirlsAloudFan":{"playlist-uri":"spotify:playlist:23gqK4kP59s8cVMwgKqMcZ"},
         "Joey788":{"playlist-uri":"spotify:playlist:0gFZRfXbImEpdO2CBRgtKY"},
         "bono_212":{"playlist-uri":"spotify:playlist:3WWFOiW5uyPv6AsXghsMrM"},
         "LJT":{"playlist-uri":"spotify:playlist:52FTbxiE1HYNBXsFvNvu4D"},
         "cobl04":{"playlist-uri":"spotify:playlist:4w1DNALRdsSX42tiNZBOAG"},
         "iron yuppie":{"playlist-uri":"spotify:playlist:1ncFpattxbIrZOJ837V2qL"}
        }

users = {
         "the tourist":{"playlist-uri":"spotify:playlist:4c9TwgH7IY2CiSi1GDmcyC"},
         "namkcuR":{"playlist-uri":"spotify:playlist:15bGW1KiB483OpGtttDuSI"},
         "GirlsAloudFan":{"playlist-uri":"spotify:playlist:23gqK4kP59s8cVMwgKqMcZ"},
         "Joey788":{"playlist-uri":"spotify:playlist:0gFZRfXbImEpdO2CBRgtKY"},
         "bono_212":{"playlist-uri":"spotify:playlist:3WWFOiW5uyPv6AsXghsMrM"},
         "LJT":{"playlist-uri":"spotify:playlist:52FTbxiE1HYNBXsFvNvu4D"},
         "cobl04":{"playlist-uri":"spotify:playlist:4w1DNALRdsSX42tiNZBOAG"},
         "iron yuppie":{"playlist-uri":"spotify:playlist:1ncFpattxbIrZOJ837V2qL"}
        }


cred = SpotifyClientCredentials(client_id="bbdfc316e42b46919a6cb7ffd26f5aad",
                 client_secret="4755d0d694084c509f73e946942aa0fb")


spotify = spotipy.Spotify(client_credentials_manager=cred)

data = {}

for user in users:

    data[user] = {"Track Listing":[], "Track Popularity":[], "Track Keys":[],
                  "Track Modes":[], "Track Time Signatures":[],
                  "Artist Genres":[], "Artist Popularity":[], "Artist Origins":[],
                  "Track Durations":[], "Track Acousticnesses":[],
                  "Track Danceabilities":[], "Track Energies":[],
                  "Track Instrumentalnesses":[], "Track Livenesses":[],
                  "Track Loudnesses":[], "Track Speechinesses":[],
                  "Track Valences":[], "Track Tempos":[],
                  "Track Segment Profiles":[], "Track Section Profiles":[]}
    
    results = spotify.playlist(users[user]["playlist-uri"])
    
    for i,track in enumerate(results['tracks']['items']):
        
        print(user+":", str(i+1) + ".",track['track']['name'])
        
        # Add artist name and song name to data
        data[user]["Track Listing"] += [{"name":track['track']['name'],
                                         "artist":track['track']['artists'][0]['name']}]
        
        data
        
        # Try to search across wikipedia for artist origin
        try:
            origin = wikisearch.try_wiki(track['track']['artists'][0]['name'])
            print("\t","Origin Detected:", origin)
        except:
            origin = None
            print("\t","Failed to detect origin.")
        
        data[user]['Artist Origins'] += [origin]
        
        # Index Spotify Track Popularity
        data[user]['Track Popularity'] += [track['track']['popularity']]
        
        # Get artist info
        artist_id = track['track']['artists'][0]['id']
        artist = spotify.artist(artist_id=track['track']['artists'][0]['id'])
        
        # Get artist ratings
        data[user]['Artist Genres'] += [artist['genres']]
        data[user]['Artist Popularity'] += [artist['popularity']]
        
        # Generate audio analysis and audio features
        aa = spotify.audio_analysis(track['track']['uri'])
        af = spotify.audio_features(track['track']['uri'])[0]
        
        # Index Track Keys 
        keys = {0:[0,'C'],
                1:[1,'C#'],
                2:[2,'D'],
                3:[3,'D#'],
                4:[4,'E'],
                5:[5,'F'],
                6:[6,'F#'],
                7:[7,'G'],
                8:[8,'G#'],
                9:[9,'A'],
                10:[10,'A#'],
                11:[11,'B']}
        
        data[user]['Track Keys'] += [keys[af['key']]]
        
        # Get track duration in seconds
        data[user]['Track Durations'] += [af['duration_ms']/1000]
        
        # Index Track Modes
        modes = {0:[0,'minor'], 1:[1,'major']}
        data[user]['Track Modes'] += [modes[af['mode']]]
        
        # Get time signature
        data[user]['Track Time Signatures'] += [af['time_signature']]
        
        # Get track acousticness
        data[user]['Track Acousticnesses'] += [af['acousticness']]
        
        # Get track danceability
        data[user]['Track Danceabilities'] += [af['danceability']]
        
        # Get track energy
        data[user]['Track Energies'] += [af['energy']]
        
        # Get track instrumentalness
        data[user]['Track Instrumentalnesses'] += [af['instrumentalness']]
        
        # Get track liveness
        data[user]['Track Livenesses'] += [af['liveness']]
        
        # Get track loudness
        data[user]['Track Loudnesses'] += [af['loudness']]
        
        # Get track speechiness
        data[user]['Track Speechinesses'] += [af['speechiness']]
        
        # Get track valence
        data[user]['Track Valences'] += [af['valence']]
        
        # Get track tempo
        data[user]['Track Tempos'] += [af['tempo']]
        
        # Gather data from audio analysis on segments
        if 'segments' in aa:
            for segment in aa['segments']:
                data[user]['Track Segment Profiles'] += [segment]
        else:
            data[user]['Track Segment Profiles'] += [[]]
        # Gather data from audio analysis on segments
        if 'sections' in aa:
            for section in aa['sections']:
                data[user]['Track Section Profiles'] += [section]
        else:
            data[user]['Track Section Profiles'] += [[]]
    with open("DIXI_data"+user+".json", "w") as f:
        json.dump(data, f)
    
    data = {}