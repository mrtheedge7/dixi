# -*- coding: utf-8 -*-
"""
Created on Sun May  3 22:33:16 2020

@author: chris
"""

import os
import json
from countryinfo import CountryInfo
import csv

data_files = [x for x in os.listdir('./')if 'cappend.json' in x]

with open('world-countries.json', 'r') as f:
    wc_data = json.load(f)


country_data = {}
country_populations = {}

country_data_user = {}

for data_file in data_files:
    
    with open(data_file, 'r') as f:
        data = json.load(f)
        
        
    user = list(data.keys())[0]
    
    for i,track in enumerate(data[user]['Track Listing']):
        
        if data[user]['Artist Origins'][i] not in country_data:
            country_data[data[user]['Artist Origins'][i]] = 0
        if data[user]['Artist Origins'][i] not in country_data_user:
            country_data_user[data[user]['Artist Origins'][i]] = 0
            
            if data[user]['Artist Origins'][i] == 'United States of America':
                use = 'United States'
            elif data[user]['Artist Origins'][i] == 'Vatican City':
                use = 'Tuvalu'
            else:
                use = data[user]['Artist Origins'][i]
            
            country_populations[data[user]['Artist Origins'][i]] = CountryInfo(use).population()
            if use == 'Tuvalu':
                country_populations[data[user]['Artist Origins'][i]] = 999999999
                
        country_data[data[user]['Artist Origins'][i]] += 1
        country_data_user[data[user]['Artist Origins'][i]] += 1
        
        for j,feature in enumerate(wc_data['features']):
            
            if 'users' not in wc_data['features'][j]['properties']:
                    wc_data['features'][j]['properties']['users'] = ''
            if 'artists' not in wc_data['features'][j]['properties']:
                wc_data['features'][j]['properties']['artists'] = []
            
            if 'artists-count' not in wc_data['features'][j]['properties']:
                wc_data['features'][j]['properties']['artists-count'] = 0
            if 'total-hits' not in wc_data['features'][j]['properties']:
                wc_data['features'][j]['properties']['total-hits'] = 0
            if 'users-count' not in wc_data['features'][j]['properties']:
                wc_data['features'][j]['properties']['users-count'] = 0
            if user +' artists' not in wc_data['features'][j]['properties']:
                wc_data['features'][j]['properties'][user +' artists'] = ''
            
            if wc_data['features'][j]['properties']['name'] == data[user]['Artist Origins'][i]:
                
                if user not in wc_data['features'][j]['properties']['users']:
                    wc_data['features'][j]['properties']['users'] += user + '<br>'
                    wc_data['features'][j]['properties']['users-count'] += 1
                wc_data['features'][j]['properties']['artists'] += [data[user]['Track Listing'][i]['artist']]
                wc_data['features'][j]['properties']['artists-count'] = len(set(wc_data['features'][j]['properties']['artists']))
                wc_data['features'][j]['properties']['total-hits'] = len(wc_data['features'][j]['properties']['artists'])
                
                
                if data[user]['Track Listing'][i]['artist'] not in wc_data['features'][j]['properties'][user +' artists']:
                    wc_data['features'][j]['properties'][user +' artists'] += data[user]['Track Listing'][i]['artist']+'<br>'
                
    
    countries_user = ['Country']
    countries_count_user = ['Count']
    countries_count_popadjust_user = ['PopAdjusted']
    for country in country_data_user:
        countries_user += [country]
        countries_count_user += [country_data_user[country]]
        countries_count_popadjust_user += [ country_data_user[country] / country_populations[country] ]
    
    
    countries_count_popadjust_user = ['PopAdjusted'] + [round(float(x)/max(countries_count_popadjust_user[1:])**(1/1),4) for x in countries_count_popadjust_user[1:]]
    
    rows = zip(countries_user, countries_count_user, countries_count_popadjust_user)
    with open(os.path.splitext(data_file)[0] + '_countries.csv', "w", newline='') as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow(row)
    countries_user = []
    countries_count_user = []
    countries_count_popadjust_user = []
    
    country_data_user = {}

countries_all = ['Country']
countries_count_all = ['Count']
countries_count_popadjust_all = ['PopAdjusted']
for country in country_data:
    countries_all += [country]
    countries_count_all += [country_data[country]]
    countries_count_popadjust_all += [ country_data[country] / country_populations[country] ]


countries_count_popadjust_all = ['PopAdjusted'] + [round((float(x)/max(countries_count_popadjust_all[1:]))**(1/1),4) for x in countries_count_popadjust_all[1:]]
    
rows = zip(countries_all, countries_count_all, countries_count_popadjust_all)

with open('DIXIdata_countries.csv', "w", newline='') as f:
    writer = csv.writer(f)
    for row in rows:
        writer.writerow(row)


with open('world-countries-adjusted.json', 'w') as f:
    json.dump(wc_data, f)