# -*- coding: utf-8 -*-
"""
Created on Sun May  3 20:39:16 2020

@author: chris
"""

import os
import json

data_files = [x for x in os.listdir('./') if 'DIXI_data' in x]
data_files = [x for x in data_files if 'cappend' not in x]
for data_file in data_files:

    with open(data_file, 'r') as f:
        data = json.load(f)
    
    user = list(data.keys())[0]
    for i,track in enumerate(data[user]['Track Listing']):
        print(data[user]['Track Listing'][i]['artist']+':')
        print('Origin...')
        print(data[user]['Artist Origins'][i])
        
        uin = input("Is this input true...:")
        
        if uin == "y":
            if data[user]['Artist Origins'][i] in ['u.s.', 'us', 'usa', 'u.s.a.', 'u.s', 'united states', 'united states of america']:
                x = 'United States of America'
            elif data[user]['Artist Origins'][i] in ['scotland', 'england', 'france', 'united kingdom', 'uk', 'u.k.', 'wales']:
                x = 'United Kingdom'
            else:
                x = data[user]['Artist Origins'][i].title()
        else:
            x = uin
        
        
        data[user]['Artist Origins'][i] = x
    
    print('saving...')

    with open(os.path.splitext(data_file)[0] + '_cappend.json' , 'w') as f:
        json.dump(data, f)